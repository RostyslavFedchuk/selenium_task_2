package com.epam.bo;

import com.epam.utils.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import java.util.List;
import java.util.ResourceBundle;

public class EmailSenderBOTest {
    private static ResourceBundle bundle = ResourceBundle.getBundle("config");
    private static final Logger LOGGER = LogManager.getLogger(EmailSenderBOTest.class);

    @Test
    public void testSendEmail() {
        EmailSenderBO sender = new EmailSenderBO();
        sender.authorize(bundle.getString("SENDER_LOGIN"),
                bundle.getString("SENDER_PASSWORD"));
        sender.sendEmail(bundle.getString("EMAIL_RECEIVER"),"WELCOME TO EPAM");
        WebDriver driver = WebDriverManager.getDriver();

        List<WebElement> showMessageButton = driver.findElements(By.id("link_vsm"));
        if(showMessageButton.isEmpty()){
            throw new AssertionError("Message is not sent!\n");
        } else {
            showMessageButton.get(0).click();
            LOGGER.info("Message was successfully sent!\n");
        }
    }
}
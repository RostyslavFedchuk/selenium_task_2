package com.epam.bo;

import com.epam.po.GmailPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EmailSenderBO {
    private static final Logger LOGGER = LogManager.getLogger(EmailSenderBO.class);
    private GmailPage gmail = new GmailPage();

    public void authorize(String login, String password) {
        LOGGER.info("Navigating to gmail.com...\n");
        gmail.navigateToGmail();
        gmail.fillLogin(login);
        gmail.submitLogin();
        LOGGER.info("Filling login...\n");
        gmail.fillPassword(password);
        gmail.submitPassword();
        LOGGER.info("Filling password...\n");
    }

    public void sendEmail(String receiver, String message) {
        LOGGER.info("Trying to write a message...\n");
        gmail.clickWriteButton();
        gmail.fillReceiverTextarea(receiver);
        gmail.fillMessageField(message);
        LOGGER.info("Filling receiver textarea and message field...\n");
        gmail.clickSendButton();
        LOGGER.info("Sending email to " + receiver + "...\n");
    }
}

package com.epam.po;

import com.epam.utils.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GmailPage {
    private WebDriver driver;
    @FindBy(id = "identifierId")
    private WebElement loginInput;
    @FindBy(xpath = "//*[@id=\"password\"]/div[1]/div/div[1]/input")
    private WebElement passwordInput;
    @FindBy(css = "span.RveJvd.snByac")
    private WebElement submitLoginButton;
    @FindBy(id = "passwordNext")
    private WebElement submitPasswordButton;
    @FindBy(xpath = "//div[text() = 'Написати']")
    private WebElement writeButton;
    @FindBy(css = "textarea[name='to']")
    private WebElement receiverTextarea;
    @FindBy(xpath = "//div[@class='Ar Au']/*[@role='textbox']")
    private WebElement messageField;
    @FindBy(xpath = "//div[@class='dC']/*[@role='button']")
    private WebElement sendButton;

    public GmailPage(){
        driver = WebDriverManager.getDriver();
        PageFactory.initElements(driver, this);
    }

    public void navigateToGmail(){
        driver.get("https://mail.google.com/mail");
    }

    public void fillLogin(String login){
        loginInput.sendKeys(login);
    }

    public void submitLogin(){
        submitLoginButton.click();
    }

    public void fillPassword(String password){
        passwordInput.sendKeys(password);
    }

    public void submitPassword(){
        submitPasswordButton.click();
    }

    public void clickWriteButton(){
        writeButton.click();
    }

    public void fillReceiverTextarea(String receiver){
        receiverTextarea.sendKeys(receiver);
    }

    public void fillMessageField(String message){
        messageField.sendKeys(message);
    }

    public void clickSendButton(){
        sendButton.click();
    }
}
